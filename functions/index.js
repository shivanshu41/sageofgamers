const functions = require('firebase-functions');
const express = require('express');
const app = express();
const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const JWT = require('jsonwebtoken');
const secret = require('./serviceAccountKey.json');
const fetch = require('node-fetch');
const steamApiKey = '84CFD7A0F3BCD316F382A150804E4A73';
const prateekID = '76561198074695865';
const steamConverter = require('steam-id-convertor');
const prateeKSteam32 = parseInt(prateekID) - 76561197960265728;
console.log('Steam 32 ', steamConverter.to32(prateekID));
const prateekID2 = '76561198296542536';
const dummyId = '76561197960434622';
const admin = require('firebase-admin');
const { response } = require('express');
admin.initializeApp();
const db = admin.firestore();
db.settings({ ignoreUndefinedProperties: true });
const storeApi = express();
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (obj, done) {
  done(null, obj);
});
passport.use(new SteamStrategy({
  returnURL: 'https://us-central1-yellocanaryhome-d868d.cloudfunctions.net/steam/auth/steam/return',
  realm: 'https://us-central1-yellocanaryhome-d868d.cloudfunctions.net/',
  apiKey: '84CFD7A0F3BCD316F382A150804E4A73'
},
  function (identifier, profile, done) {
    if (profile) {
      user = profile;
      console.log("GOT user");
      console.log(user);
      return done(null, user);
    }
    else {
      return done(null, false);
    }
  }
));
const setNewUser = (req, res, next) => {
  console.log("Set user");
  console.log(req.user._json);
  next();
}
app.use(passport.initialize());
app.get('/', (req, res) => {
  console.log(req.user);
  res.send('Hello firebase');
})
app.get('/auth/steam',
  passport.authenticate('steam'),
  function (req, res) {
    console.log("Steaming");
    console.log(res);
    res.end();
  });
app.get('/auth/steam/return',
  passport.authenticate('steam', { failureRedirect: '/login' }),

  async function (req, res) {
    // Successful authentication, redirect home.
    console.log("Steam id ", req.user._json.steamid);
    const jwtOptions = {
      algorithm: 'RS256',
      issuer: secret.client_email,
      subject: secret.client_email,
      audience: 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
      expiresIn: 3600,
    }
    const token = JWT.sign({ uid: req.user._json.steamid }, secret.private_key, jwtOptions);
    console.log(token);
    let userData = req.user._json;
    let dotaId = steamConverter.to32(req.user._json.steamid);
    if (token) {
      let docRef = db.collection('users').doc(userData.steamid);
      let promisesToKeep = [];
      promisesToKeep.push(
        docRef.set({
          displayName: userData.personaname,
          avatar: userData.avatarfull,
          lastLogOff: userData.lastlogoff,
          realName : userData.realname,
          countryCode : userData.loccountrycode,
          uid : userData.steamid
        })
          .catch((e) => {
            console.log(e);
          })
      );
      promisesToKeep.push(
        fetch(`https://api.opendota.com/api/players/${dotaId}`)
          .then((dotaResponse) => {
            return dotaResponse.json()
          })
          .then((dotaResults) => {
            let dotaInfo = dotaResults;
            // This ref stores a collection of games with foreignKey steamid refering to "User" collection
            let dotaRef = db.collection('games').doc('dota2').collection('players').doc(dotaInfo.profile.steamid)
            let mainRankToString = dotaInfo.rank_tier.toString();
            let mainRank = mainRankToString[0];
            return dotaRef.set({
              personaname: dotaInfo.profile.personaname,
              dotaId: dotaInfo.profile.account_id,
              dota_avatar: dotaInfo.profile.avatarfull,
              profileUrl: dotaInfo.profile.profileurl,
              ranktier: dotaInfo.rank_tier,
              mainRank : parseInt(mainRank),
              steamId: dotaInfo.profile.steamid,
            }, { merge: true })
            .then((response)=>{
              console.log("Dota values set");
              console.log(response);
            })
            .catch((e)=>{
              console.log(e);
            })
          })
          .catch((e) => {
            console.log(e);
          })
      )
      promisesToKeep.push(
        fetch(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${steamApiKey}&steamid=${userData.steamid}&format=json&include_appinfo=true&include_played_free_games=true`)
          .then((response) => {
            return response.json();
          })
          .then((result) => {
            console.log(result);
            // Filtering Dota 2 from array of owned games 
            result.response.games.map((value, index) => {
              // 570 is the id for dota2
              if (value.appid === 570) {
                let dotaRef = db.collection('games').doc('dota2').collection('players').doc(userData.steamid)
                dotaRef.set({
                  hoursPlayed: value.playtime_forever,
                  appId: value.appid,
                }, { merge: true })
                .then((response)=>{
                  console.log("Hours Played and appid merged into dota values");
                  console.log(response);
                })
                .catch((e)=>{
                  console.log(e);
                })
              } else {
                return value;
              }
            })
          })
      )
      return Promise.all(promisesToKeep)
        .then((allResponses) => {
          allResponses.forEach((value,index)=>{
            console.log(value);
          })
          res.redirect(`http://sageofgamers1.web.app/login?token=${token}`);
        })
        .catch((e) => {
          console.log(e);
          res.status(500).send("Internal server error");
        })
    }
  });

// Decode Rank method

const decodeRank = (ranktier) => {
  if (typeof ranktier == Number) {
    console.log('Rank tier is ', ranktier);

  }
  else {
    console.log("ranktier is not a number ", ranktier);
    return false
  }
}

app.get('/friendList', (req, res) => {
  fetch(`http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key=${steamApiKey}&steamid=${req.query.steamId}&relationship=friend`)
    .then((result) => {
      return result.json();
    })
    .then((result) => {
      console.log(result);
      res.json(result);
    })
    .catch((e) => {
      console.log(e);
      res.status(500);
    })
})

app.get('/ownedGames', (req, res) => {
  fetch(`http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=${steamApiKey}&steamid=${prateekID}&format=json&include_appinfo=true&include_played_free_games=true`)
    .then((result) => {
      return result.json();
    })
    .then((result) => {
      console.log(result);
      // Filtering games with name Dota 
      let map = { searchedResults: [], mainResults: result };
      result.response.games.map((value, index) => {
        if (value.name.search('Dota') !== -1) {
          map.searchedResults.push(value);
        } else {
          return value;
        }
      })
      console.log(`${map.searchedResults.length} Dota items found !`);
      res.json(map);
    })
    .catch((e) => {
      console.log(e);
      res.status(500);
    })
})
app.get('/recentlyPlayedGames', (req, res) => {
  fetch(`http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=${steamApiKey}&steamid=${prateekID2}&format=json`)
    .then((result) => {
      return result.json();
    })
    .then((result) => {
      console.log(result);
      // Filtering games with name Dota 
      let map = { searchedResults: [], mainResults: result };
      result.response.games.map((value, index) => {
        if (value.name.search('Dota') !== -1) {
          map.searchedResults.push(value);
        } else {
          return value;
        }
      })
      console.log(`${map.searchedResults.length} Dota items found !`);
      res.json(map);
    })
    .catch((e) => {
      console.log(e);
      res.status(500);
    })
})

app.get('/games/dota2/playerInfo', (req, res) => {
  let steam32 = steamConverter.to32(prateekID)
  fetch(`https://api.opendota.com/api/players/${steam32}`)
    .then((response) => {
      return response.json();
    })
    .then((result) => {
      console.log(result);
      res.json(result);
    })
    .catch((e) => {
      res.status(500);
    })
})

exports.steam = functions.https.onRequest(app);