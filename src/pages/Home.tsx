import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import Button from '@material-ui/core/Button';
import './Home.css';
import { ReactComponent as Logo } from '../images/vector.svg';
import { ReactComponent as HomeIcon } from '../images/home.svg';
import { ReactComponent as JoystickIcon } from '../images/joystick.svg';
import { ReactComponent as MessageIcon } from '../images/message.svg';
import { ReactComponent as RatingIcon } from '../images/rating.svg';
import { ReactComponent as DancingIcon } from '../images/dancing.svg';
import { ReactComponent as InstaIcon } from '../images/instagram.svg';
import { ReactComponent as FacebookIcon } from '../images/facebook.svg';
const Home: React.FC = () => {
  return (
    <>
      <div className='homeContainer'>
        <div className='hero'>
          <div className='leftHero'>
            <div className='logo'>
              <Logo />
            </div>
            <Button variant="contained" color="primary" size={'large'} style={{ boxShadow: "0px 0px 15px 15px rgb(191, 195, 121)", color: "#ffffff",margin:"1em 0" }}>
              FIND TEAMMATES
            </Button>
          </div>
          <div className='rightHero'>
            <h1>THE <span style={{ color: "rgb(191, 195, 121)" }}>BEST PLATFORM</span> TO FIND TEAMMATES</h1>
            
          </div>
        </div>
        <div className='section1'>
          <div className='s1p1'>
            <div className='s1p1p1'>
              <JoystickIcon className={'secIcon'} />
            </div>
            <div className='s1p1p2'>
              <h2 style={{ fontFamily: 'Goldman,cursive' }}>FIND FRIENDS</h2>
              <div style={{ fontSize: '16px', fontFamily: "Open Sans,sans-serif" }}>We have a detailed search system. You can filter players to find the best teammate for you.</div>
            </div>
          </div>
          <div className='s1p2'>
            <div className='s1p2p1'>
              <MessageIcon className={'secIcon'} />
            </div>
            <div className='s1p2p2'>
              <h2 style={{ fontFamily: 'Goldman,cursive' }}>CHAT WITH FRIENDS</h2>
              <div style={{ fontSize: '16px', fontFamily: "Open Sans,sans-serif" }}>Get notified when your friend request is accepted and chat with them rightaway in real time !</div>
            </div>
          </div>
        </div>
        <div className='section2'>
          <div className='s2p1'>
            <div className='s2p1p1'>
              <RatingIcon className={'secIcon'} />
            </div>
            <div className='s2p1p2'>
              <h2 style={{ fontFamily: 'Goldman,cursive' }}>RATING SYSTEM</h2>
              <div style={{ fontSize: '16px', fontFamily: "Open Sans,sans-serif" }}>You can see other players ratings and once you got matched and played a game with a player, you can rate them in 2 days or 20 matches.</div>
            </div>
          </div>
          <div className='s2p2'>
            <div className='s2p2p1'>
              <DancingIcon className={'secIcon'} />
            </div>
            <div className='s2p2p2'>
              <h2 style={{ fontFamily: 'Goldman,cursive' }}>HAVE FUN</h2>
              <div style={{ fontSize: '16px', fontFamily: "Open Sans,sans-serif" }}>You will have tons of fun with the best teammates ever. Games are meant to be fun after all :)</div>
            </div>
          </div>
        </div>
        <div className='section3'>
          <div className='s3p1'>
            <div style={{ flex: 1, display: 'flex', justifyContent: 'center', alignItems: "center" }}>
              <img className={'s3Img'} style={{ flex: 1 }} src='../images/dota-image.png' />
            </div>
          </div>
          <div className='s3p2'>
            <div className='s3p2p1'>
              <div style={{ fontSize: "30px", fontFamily: "Goldman,cursive", fontWeight: 600 }}>
                DOTA 2
              </div>
              <div style={{ fontFamily: "Open Sans,sans-serif" }}>
                Dota 2 is a multiplayer online battle arena video game developed and published by Valve.
              </div>
              <div>
                <Button variant="contained" color="primary" size={'large'} style={{ boxShadow: "0px 0px 15px 15px rgb(191, 195, 121)", color: "#ffffff" }}>
                  FIND TEAMMATES
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="section4">
          <div className="sectionContainer">
            <div className="socialBox">
              <div className="socialTitle">STAY CONNECTED</div>
              <div style={{ flex: 1, display: "flex", justifyContent: "center", alignItems: "center", width: "100%" }}>
                <FacebookIcon className="socialIcon fb" />
                <InstaIcon className="socialIcon insta" />
              </div>
            </div>
          </div>
        </div>
        <div className={'footer'}>
          COPYRIGHT © 2020
        </div>
      </div>
    </>
  );
};

export default Home;
