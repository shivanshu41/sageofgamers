import React, { useContext, useEffect, useState } from 'react';
import firebase from '../firebase';
import './Login.css';
import { AuthContext } from '../authContext/authContext';
import Dashboard from '../components/Dashboard/Dashboard';
import { Button } from '@material-ui/core';
const Login: React.FC = () => {
    const { authUser, logout } = useContext(AuthContext);
    const [initLogin, setInitLogin] = useState(false);
    // return <div style={{padding:"10em"}}>
    //  User is {authUser ? authUser.uid : "no user"}    
    //  <br/>
    //  {!authUser ? <button onClick={()=>{window.location.href = 'https://us-central1-yellocanaryhome-d868d.cloudfunctions.net/steam/auth/steam'}}>Login</button>:<button onClick={()=>{firebase.auth().signOut().then(()=>window.location.href='/login')}}>logout</button>}
    // <br/><br/>

    // </div>
    return <>
        {authUser ? <Dashboard /> : <><Button variant={'contained'} size={'large'}
            style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%,-50%)",
                color: "#ffffff",
                fontFamily: 'Goldman,cursive',
                fontWeight: 600,
                background: "linear-gradient(90deg, rgba(23,26,33,1) 0%, rgba(42,71,94,1) 20%, #1b2838 60% , #2a475e 80% ,rgba(199,213,224,1) 100%)",
            }} onClick={() => { setInitLogin(true) ;  window.location.href = 'https://us-central1-yellocanaryhome-d868d.cloudfunctions.net/steam/auth/steam' }}>Login with steam</Button>
            <div className={initLogin ? "spinner" : "spinnerContainer"} style={{marginTop:"5em",position:"absolute",top:"50%",left:"50%",transform:"translate(-50%,-50%)"}} />
        </>
        }
    </>
}
export default Login;