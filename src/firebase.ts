import firebase from 'firebase';
const firebaseConfig = {
    apiKey: "AIzaSyDpGCocIKoTAsvL1XPRdXjKGImo1ZDyFvY",
    authDomain: "sageofgamers1.firebaseapp.com",
    databaseURL: "https://sageofgamers1.firebaseio.com",
    projectId: "sageofgamers1",
    storageBucket: "sageofgamers1.appspot.com",
    messagingSenderId: "996805876141",
    appId: "1:996805876141:web:3f1af68fea04e145156924",
    measurementId: "G-0F2NP6F3S3"
};
const yelloSOG = {
    apiKey: "AIzaSyAGX-Wiazc10x7HV2qENojhS0du0Wr_zec",
    authDomain: "yellocanaryhome-d868d.firebaseapp.com",
    databaseURL: "https://yellocanaryhome-d868d.firebaseio.com",
    projectId: "yellocanaryhome-d868d",
    storageBucket: "yellocanaryhome-d868d.appspot.com",
    messagingSenderId: "614417282036",
    appId: "1:614417282036:web:d015b5bed8607a4e0f7fa1"
}
// The configuration should match the private key with which the custom token is created other while logging
// in with firebase it will give error - Custom token corresponds to different audience

firebase.initializeApp(yelloSOG);
export default firebase;