import React from 'react';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Home from './pages/Home';
import Login from './pages/Login';
import Players from './pages/Players';
import ChatPage from './pages/ChatPage';
/* Theme variables */
import './theme/variables.css';
import BottomNav from './components/BottomNav';
import { ThemeProvider } from '@material-ui/core';
import Theme from './theme/theme';
import AuthProvider from './authContext/authContext';
const App: React.FC = () => (
    <Router>
        <AuthProvider>
            <ThemeProvider theme={Theme}>

                <Switch>
                    <Route exact path={'/'}>
                        <BottomNav />
                        <Home></Home>
                    </Route>
                    <Route exact path={'/login'}>
                        <BottomNav />
                        <Login></Login>
                    </Route>
                    <Route exact path={'/players'}>
                        <BottomNav />
                        <Players></Players>
                    </Route>
                    <Route exact path={'/chat'}>
                        <BottomNav />
                        <ChatPage/>
                    </Route>
                </Switch>

            </ThemeProvider>
        </AuthProvider>
    </Router>
);

export default App;
