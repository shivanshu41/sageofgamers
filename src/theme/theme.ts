import { createMuiTheme } from '@material-ui/core';


// get current theme from online store or internal storage
interface theme{

}

const theme = createMuiTheme({
    palette: {
        primary: {
          main: "rgb(191, 195, 121)",
        },
        secondary: {
          main: "#B7B998",
        },
        
      },
});

export default theme;