import React, { useEffect, useState, createRef, useContext } from 'react';
import './BottomNav.css';
import { ReactComponent as Logo } from '../images/vector.svg';
import { ReactComponent as HomeIcon } from '../images/home.svg';
import { ReactComponent as InfoIcon } from '../images/information.svg';
import { ReactComponent as VideoGameIcon } from '../images/video-game.svg';
import { ReactComponent as LoginIcon } from '../images/user.svg';
import { AuthContext } from '../authContext/authContext';
import { ReactComponent as FriendsIcon } from '../images/friends.svg';
import { ReactComponent as ChatIcon } from '../images/chat.svg';
import { Link, useLocation } from 'react-router-dom';
const BottomNav: React.FC = () => {
    const navRef = createRef();
    const location = useLocation();
    const { authUser } = useContext(AuthContext);
    console.log("Auth from BottomNav ", authUser);
    console.log(location.pathname);
    const [scroll, setScroll] = useState<null | any>({ scrollDir: '', lastScroll: 0, bottom: 0 });
    useEffect(() => {
        window.addEventListener('scroll', () => {
            console.log('scrolled');
            const currentScrollTopVal = document.body.scrollTop || document.documentElement.scrollTop;
            const lastScrollPosition = scroll.lastScroll;
            console.log('last Pos ', lastScrollPosition);
            if (lastScrollPosition > currentScrollTopVal) {
                setScroll({
                    scrollDir: 'top',
                    lastScroll: currentScrollTopVal,
                    bottom: 0
                });
            } else if (lastScrollPosition < currentScrollTopVal) {
                setScroll({
                    scrollDir: 'bottom',
                    lastScroll: currentScrollTopVal,
                    bottom: 0 // Value to which navbar hides. Set it to 0 to stop hiding
                });
            }
        })
    }, [scroll.lastScroll])
    return <>
        <div className='bottomNav' style={{ position: 'fixed', right: 0, left: 0, padding: "0.2em", top: scroll.bottom }}>
            <div className='logoText'>SAGE <span>OF</span> GAMERS</div>
            <div className='menu'>
                <div className={`navBtn ${location.pathname === '/' ? 'active' : ''}`}><Link to={'/'}><HomeIcon className={'navIcon'} /><div className='iconText'>HOME</div></Link></div>
                <div className={`navBtn ${location.pathname === '/players' ? 'active' : ''}`}><Link to={'/players'}><VideoGameIcon className={'navIcon'} /><div className='iconText'>PLAYERS</div></Link></div>
                <div className={`navBtn ${location.pathname === '/login' ? 'active dashboard' : ''}`}>
                    <div className={'dropmenu card-2'}>

                        <div className={'card-3'} >
                            <Link to={'/chat'} className={'dropmenuItem'}>FRIENDS</Link>
                        </div>
                        <div className={'card-3'} style={{ borderRadius: "0 0 10px 10px" }}>Settings</div>

                    </div>
                    <Link to={'/login'}><LoginIcon className={'navIcon'} />
                        <div className='iconText'>{authUser ? "PROFILE" : "LOGIN"}</div>
                    </Link>

                </div>
            </div>
        </div>
    </>
}
export default BottomNav