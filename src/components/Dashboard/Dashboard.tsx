import React, { useState, useContext, useEffect } from 'react';
import './Dashboard.css';
import { ReactComponent as CountryIcon } from '../../images/india.svg';
import { ReactComponent as LogOutIcon } from '../../images/logout.svg';
import { ReactComponent as ClockIcon } from '../../images/clock.svg';
import { ReactComponent as FlashIcon } from '../../images/flash.svg';
import firebase from '../../firebase';
import { Checkbox, Radio, RadioProps } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { AuthContext } from '../../authContext/authContext';
const Dashboard: React.FC = () => {
    const { authUser } = useContext(AuthContext);
    const db = firebase.firestore();
    const [userProfile, setUserProfile] = useState<null | any>(null);
    const [gamerProfile, setGamerProfile] = useState<null | any>(null);
    useEffect(() => {
        if (authUser) {
            console.log("Auth user state is ", authUser.uid);
            // setting profile Data
            const docRef = db.collection('users').doc(authUser?.uid);
            const gameDocRef = db.collection('games').doc('dota2').collection('players').doc(authUser.uid);
            docRef.get()
                .then((docSnapshot) => {
                    if (docSnapshot.exists) {
                        console.log("Doc is ", docSnapshot.data())
                        let data = docSnapshot.data();
                        setUserProfile(data);
                    }
                })
                .catch((e) => {
                    setUserProfile(null);
                })
            gameDocRef.get()
                .then((docSnapshot) => {
                    if (docSnapshot.exists) {
                        console.log("Gamer is ", docSnapshot.data())
                        let data = docSnapshot.data();
                        setGamerProfile(data);
                        setChecked(data?.mic);
                        setRole(data?.role);
                    }
                })
                .catch((e) => {
                    setGamerProfile(null);
                })

            console.log("USer profile at profile ", userProfile)
        }
    }, [])
    const [checked, setChecked] = useState(false);
    const handleCheck = (event: React.ChangeEvent<HTMLInputElement>) => {
        const updateRoleRef = db.collection('games').doc('dota2').collection('players').doc(authUser.uid);
        updateRoleRef.update({
            mic: event.target.checked
        })
            .catch((e)=>{
                console.log(e);
            })
        setChecked(event.target.checked);
    }
    const [role, setRole] = React.useState('a');

    const handleRole = (event: React.ChangeEvent<HTMLInputElement>) => {
        const updateRoleRef = db.collection('games').doc('dota2').collection('players').doc(authUser.uid);
        setRole(event.target.value);
        updateRoleRef.update({
            role: event.target.value
        })
            .catch((e)=>{
                console.log(e);
            })
    };
    const customRadio = makeStyles({
        root: {
            color: "rgb(255,255,255,0.6)"
        },
    });
    const decodeRankAndHoursPlayed = (rank: string, hoursPlayed: number) => {
        interface rankTier {
            [key: string]: string
        }
        const rankTier: rankTier = {
            1: "Herald",
            2: "Guardian",
            3: "Crusader",
            4: "Archon",
            5: "Legend",
            6: "Ancient",
            7: "Divine",
            8: "Immortal",
        }

        if (rank && hoursPlayed) {
            let mainRank = rank[0];
            console.log({
                rank: `${rankTier[mainRank]} ${rank[1]}`,
                hoursPlayed: hoursPlayed ? hoursPlayed / 60 : 0
            })
            return {
                rank: `${rankTier[mainRank]} ${rank[1]}`,
                hoursPlayed: hoursPlayed ? hoursPlayed / 60 : 0
            }
        }
    }
    const preRender = () => {
        if (userProfile && gamerProfile) {
            return <>
                <div className={'dashSideMenu'}>
                    <div className={'dashSideMenuContent'}>
                        <div className={'dashProfile'}>
                            <img className={'dashProfilePic'} src={userProfile ? userProfile.avatar : ""} />
                        </div>
                        <div className={'dashMenu'}>
                            <div className={'dashName'}>{userProfile ? userProfile.displayName : ""}</div>
                            <div className={'dashName realName'}>{userProfile ? userProfile.realName : ""}</div>
                            <div className={'dashCountry'}><CountryIcon />INDIA</div>
                            <div className={'dashRank'}>
                                <img src={"images/crusader5.png"} className={'dashRankIcon'} />
                                <div>{decodeRankAndHoursPlayed(gamerProfile.ranktier.toString(), gamerProfile.hoursPlayed)?.rank}</div>
                            </div>
                            <div className={'dashLogout card-1'} onClick={() => { firebase.auth().signOut().then(() => window.location.href = '/login') }}><LogOutIcon />LOGOUT</div>
                            <div></div>
                        </div>
                    </div>
                </div>
                <div className={'profileBody card-2'}>
                    <div className={'profileContainer'}>
                        <div className={'profile'}>
                            Hi , Welcome to <span style={{color:"rgb(191, 195, 121)"}}>Sage Of Gamers</span>
                    </div>
                        <div className={'profileHoursPlayed'}>
                            <ClockIcon />
                            <div><span style={{ fontSize: "28px", fontFamily: "Goldman,cursive", fontWeight: 600 }}>
                                {decodeRankAndHoursPlayed(gamerProfile.ranktier.toString(), gamerProfile.hoursPlayed)?.hoursPlayed}
                            </span> Hours played in total</div>
                        </div>
                        <div className={'isMic'}>
                            Do you have a microphone ?
                        <Checkbox
                                color='primary'
                                classes={{
                                    root: classes.root
                                }}
                                checked={checked}
                                onChange={handleCheck}
                                inputProps={{ 'aria-label': 'primary checkbox' }}
                            />
                        </div>
                        <div className={'selectRole'}>
                            <div className="roleTitle"><FlashIcon />Select the role you want to play</div>
                            <div className='roles'>
                                <div className={'role'}><Radio
                                    classes={{
                                        root: classes.root
                                    }}
                                    color="primary"
                                    checked={role === 'hardSupport'}
                                    onChange={handleRole}
                                    value="hardSupport"
                                    name="radiorole"
                                    inputProps={{ 'aria-label': 'hardSupport' }}
                                />Hard Support</div>
                                <div className={'role'}><Radio
                                    classes={{
                                        root: classes.root
                                    }}
                                    checked={role === 'softSupport'}
                                    onChange={handleRole}
                                    value="softSupport"
                                    color="primary"
                                    name="radioRole"
                                    inputProps={{ 'aria-label': 'softSupport' }}
                                />Soft Support</div>
                                <div className={'role'}><Radio
                                    classes={{
                                        root: classes.root
                                    }}
                                    color="primary"
                                    checked={role === 'safeLane'}
                                    onChange={handleRole}
                                    value="safeLane"
                                    name="radioRole"
                                    inputProps={{ 'aria-label': 'safeLane' }}
                                />Safe Lane</div>
                                <div className={'role'}>
                                    <Radio
                                        classes={{
                                            root: classes.root
                                        }}
                                        color="primary"
                                        checked={role === 'midLane'}
                                        onChange={handleRole}
                                        value="midLane"
                                        name="radioRole"
                                        inputProps={{ 'aria-label': 'midLane' }}
                                    />Mid Lane</div>
                                <div className={'role'}>
                                    <Radio
                                        classes={{
                                            root: classes.root
                                        }}
                                        color="primary"
                                        checked={role === 'offLane'}
                                        onChange={handleRole}
                                        value="offLane"
                                        name="radioRole"
                                        inputProps={{ 'aria-label': 'OffLane' }}
                                    />Off Lane</div>
                            </div>
                        </div>

                    </div>
                </div>
            </>
        } else {
            return <div className={'spinner'} style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%,-50%)" }} />
        }
    }
    const classes = customRadio();
    return <>

        <div className={'dashContainer'}>
            {preRender()}
        </div>
    </>
}
export default Dashboard