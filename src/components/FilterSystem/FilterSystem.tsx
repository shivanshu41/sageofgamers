import React, { useState, useContext, useEffect } from 'react';
import PlayerCard from './PlayerCard';
import { ReactComponent as ToxicityIcon } from '../../images/biohazard.svg';
import { ReactComponent as Dota2Icon } from '../../images/dota-2.svg';
import { ReactComponent as ClockIcon } from '../../images/clock.svg';
import { ReactComponent as RankIcon } from '../../images/ranking.svg';
import { Select, MenuItem, InputLabel, FormControl, Slider, TextField, Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { AuthContext } from '../../authContext/authContext';
import './FilterSystem.css';
import firebase from '../../firebase';
import { parse } from 'path';
interface userScope {
    scopeName: 'friendList' | 'playerAchievements' | 'userStatsForGame' | 'recentlyPlayedGames'
}
interface fetchedPlayer {
    displayName: string,
    avatar: string,
    rank: string,
    hoursPlayed: number,
}
function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const FilterSystem = () => {
    const [state, setState] = useState<{ [scope in userScope["scopeName"]]: string }>({
        friendList: "",
        playerAchievements: "",
        userStatsForGame: "",
        recentlyPlayedGames: ""
    });
    const { authUser } = useContext(AuthContext);
    const [player, setPlayer] = useState<Array<any>>([]);
    const [game, setGame] = useState<string | null>('dota 2');
    useEffect(() => {
        // search only for Dota 2 app id 570
        if (authUser) { 
            firebase.firestore().collection('games').doc('dota2').collection('players').where("appId", "==", 570).where("steamId", '!=', authUser.uid)
                .get()
                .then((snapshot) => {
                    console.log(snapshot.docs);
                    if (!snapshot.empty) {
                        let fetchedPlayers = player;
                        snapshot.docs.forEach((value, index) => {
                            console.log(value.data())
                            let playerSnap = value.data();
                            setPlayer((prevState) => {
                                return prevState.concat(playerSnap);
                            });
                        })

                    }
                })
                .catch((e) => {
                    console.log(e);
                })
        }
    }, [authUser])


    const executeRankFilter = () => {
        console.log("Executing rank filter between ", minRank, " ", maxRank);
        firebase.firestore().collection('games').doc('dota2').collection('players').where("appId", "==", 570).where('mainRank', '>=', minRank).where('mainRank', '<=', maxRank).get()
            .then((snapshot) => {
                console.log(snapshot.docs);

                let filteredPlayers: any = [];
                snapshot.docs.forEach((value, index) => {
                    console.log(value.data())
                    let playerSnap = value.data();
                    filteredPlayers.push(playerSnap);
                })
                setPlayer(filteredPlayers);


            })
            .catch((e) => {
                console.log(e);
            })
    }


    const executeHoursPlayedFilter = () => {
        console.log("Executing hoursPlayed filter");
        firebase.firestore().collection('games').doc('dota2').collection('players').where("appId", "==", 570).where('hoursPlayed', '>=', hoursPlayedMin).where('hoursPlayed', '<=', hoursPlayedMax).get()
            .then((snapshot) => {
                console.log(snapshot.docs);
                let filteredPlayers: any = [];
                snapshot.docs.forEach((value, index) => {
                    console.log(value.data())
                    let playerSnap = value.data();
                    filteredPlayers.push(playerSnap);
                })
                setPlayer(filteredPlayers);
            })
            .catch((e) => {
                console.log(e);
            })
    }


    const [minRank, setMinRank] = useState(1);
    const [maxRank, setMaxRank] = useState(8);
    const handleRank = (event: React.ChangeEvent<{ value: unknown }>, natureOfRank: "min" | "max") => {
        if (natureOfRank === 'min') {
            setMinRank(event.target.value as number);
        }
        else if (natureOfRank === 'max') {
            setMaxRank(event.target.value as number);
        }
    }
    const [hoursPlayedMax, setHoursPlayedMax] = useState<number>(100000);
    const [hoursPlayedMin, setHoursPlayedMin] = useState<number>(0);
    const handleHoursPlayed = (event: any, natureOfHoursPlayed: "min" | "max") => {
        if (natureOfHoursPlayed === 'min') {
            setHoursPlayedMin(event.target.value as number);
        }
        else if (natureOfHoursPlayed === 'max') {
            setHoursPlayedMax(event.target.value as number);
        }
    }

    interface rankTier {
        [key: string]: string
    }
    const rankTier: rankTier = {
        1: "Herald",
        2: "Guardian",
        3: "Crusader",
        4: "Archon",
        5: "Legend",
        6: "Ancient",
        7: "Divine",
        8: "Immortal",
    }
    const decodeRankAndHoursPlayed = (rank: string, hoursPlayed: number) => {

        let mainRank = rank[0];
        return {
            rank: `${rankTier[mainRank]} ${rank[1]}`,
            hoursPlayed: hoursPlayed / 60
        }
    }
    const [alertSnack, setAlertSnack] = useState(false);
    const handleSnackClose = () => {
        setAlertSnack(false);
    }
    const handleSnackOpen = () => {
        console.log("Snack open");
        setAlertSnack(true);
    }
    const handleAddFriend = () => {
        if (!authUser) {
            handleSnackOpen();
        }
        else if (authUser) {
            console.log(authUser);
            alert('friennd Added')
        }
    }
    const handlePlayerGrid = player.map((value, index) => {
        let { rank, hoursPlayed } = decodeRankAndHoursPlayed(value.ranktier.toString(), value.hoursPlayed);
        return <PlayerCard key={`player_${index}`} player={{
            rank: rank,
            hoursPlayed: Math.floor(hoursPlayed),
            roles: value.role ? [value.role] : [""],
            mic: value.mic ? value.mic : false,
            uid: value.steamId,
            profilePic: value.dota_avatar,
            displayName: value.personaname
        }} />
    });

    return <>
        <Snackbar open={alertSnack} autoHideDuration={5000} onClose={handleSnackClose}>
            <Alert onClose={handleSnackClose} severity="warning">
                Please sign in to use this feature !
        </Alert>
        </Snackbar>
        <div className={'filterContainer'}>

            <div className={'sideMenu'}>
                <div className={'sideMenuContent'}>
                    <div className={'gameName'}>
                        <Dota2Icon style={{ height: "1em", width: "1em", marginRight: "0.5em" }} />
                        <div>Dota 2</div>
                    </div>
                    <div style={{ background: "#52a186", padding: "1em", fontFamily: "Goldman,cursive", fontWeight: 600, fontSize: "20px" }}>
                        FIND YOUR TEAMMATES
                    </div>
                    <div className={'filterTitle'}>
                        <RankIcon style={{ height: "1em", width: "1em", marginRight: "0.5em", fill: "#E91E63" }} />
                        <div>RANK</div>
                    </div>
                    <div className={'filterContent'}>
                        <div className={'rankFilter'}>
                            <div style={{ color: "#f06292" }}>MIN</div>
                            <FormControl style={{ width: "100%" }}>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={minRank}
                                    onChange={(e) => { handleRank(e, 'min') }}
                                    style={{ width: "100%", color: "#ffffff" }}
                                    autoWidth
                                    onBlur={executeRankFilter}
                                >
                                    <MenuItem value={'default'} disabled>Choose Rank</MenuItem>
                                    <MenuItem value={1}>{rankTier[1]}</MenuItem>
                                    <MenuItem value={2}>{rankTier[2]}</MenuItem>
                                    <MenuItem value={3}>{rankTier[3]}</MenuItem>
                                    <MenuItem value={4}>{rankTier[4]}</MenuItem>
                                    <MenuItem value={5}>{rankTier[5]}</MenuItem>
                                    <MenuItem value={6}>{rankTier[6]}</MenuItem>
                                    <MenuItem value={7}>{rankTier[7]}</MenuItem>
                                    <MenuItem value={8}>{rankTier[8]}</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <div className={'rankFilter'}>
                            <div style={{ color: "#f06292" }}>MAX</div>
                            <FormControl style={{ width: "100%" }}>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={maxRank}
                                    onChange={(e) => { handleRank(e, 'max') }}
                                    style={{ width: "100%", color: "#ffffff" }}
                                    autoWidth
                                    onBlur={executeRankFilter}
                                >
                                    <MenuItem value={'default'} disabled>Choose Rank</MenuItem>
                                    <MenuItem value={1}>{rankTier[1]}</MenuItem>
                                    <MenuItem value={2}>{rankTier[2]}</MenuItem>
                                    <MenuItem value={3}>{rankTier[3]}</MenuItem>
                                    <MenuItem value={4}>{rankTier[4]}</MenuItem>
                                    <MenuItem value={5}>{rankTier[5]}</MenuItem>
                                    <MenuItem value={6}>{rankTier[6]}</MenuItem>
                                    <MenuItem value={7}>{rankTier[7]}</MenuItem>
                                    <MenuItem value={8}>{rankTier[8]}</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                    </div>

                    <div className={'filterTitle'}>
                        <ClockIcon style={{ height: "1em", width: "1em", marginRight: "0.5em", fill: "#FF5722" }} />
                        <div>HOURS PLAYED</div>
                    </div>
                    <div className={'filterContent'}>
                        <div className={'hoursFilter'}>
                            <div style={{ color: "#FFCCBC" }}>MIN</div>
                            <TextField
                                classes={{ root: "hoursPlayedTextField" }}
                                id="hoursPlayedMin"
                                type="number"
                                placeholder="No. of hours"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={hoursPlayedMin}
                                onChange={(e) => handleHoursPlayed(e, 'min')}
                                onBlur={executeHoursPlayedFilter}
                            />
                        </div>
                    </div>
                    <div className={'filterContent'}>
                        <div className={'hoursFilter'}>
                            <div style={{ color: "#FFCCBC" }}>MAX</div>
                            <TextField
                                classes={{ root: "hoursPlayedTextField" }}
                                id="hoursPlayedMax"
                                type="number"
                                placeholder="No. of hours"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={hoursPlayedMax}
                                onChange={(e) => handleHoursPlayed(e, "max")}
                                onBlur={executeHoursPlayedFilter}
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className={'dashBody card-2'}>
                <div className={'playerGrid'}>
                    {handlePlayerGrid}
                </div>
                <div className={'pagination'}>
                    <div className={'card-1'}>{`<`}</div>
                    <div className={'card-1'}>1</div>
                    <div className={'card-1'}>2</div>
                    <div className={'card-1'}>3</div>
                    <div className={'card-1'}>4</div>
                    <div className={'card-1'}>5</div>
                    <div className={'card-1'}>6</div>
                    <div className={'card-1'}>{`>`}</div>

                </div>
            </div>
        </div>
    </>
}
export default FilterSystem;