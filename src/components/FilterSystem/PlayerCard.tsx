import React, { useState, useContext, useEffect } from 'react';
import './PlayerCard.css';
import { ReactComponent as Dancing } from '../../images/dancing.svg';
import { ReactComponent as ToxicityIcon } from '../../images/biohazard.svg';
import { ReactComponent as ClockIcon } from '../../images/clock.svg';
import { ReactComponent as RankIcon } from '../../images/ranking.svg';
import { ReactComponent as MicIcon } from '../../images/mic.svg';
import RankImg from '../Rank';
import { Button } from '@material-ui/core';
import { AuthContext } from '../../authContext/authContext';
import firebase from '../../firebase';
import { Select, MenuItem, InputLabel, FormControl, Slider, TextField, Snackbar } from '@material-ui/core';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
const db = firebase.firestore();
interface PlayerData {
    rank: string,
    hoursPlayed: number,
    roles: Array<string>,
    mic: boolean,
    profilePic: string,
    displayName: string,
}
interface PlayerCardProps {
    player: any
}
interface addedFriend {
    uid: string,
    status: "Pending" | "Friend"
}
const PlayerCard: React.FC<PlayerCardProps> = (props: PlayerCardProps) => {
    const player = props.player;
    const { authUser } = useContext(AuthContext);
    const [friendAdded, setFriendAdded] = useState<"Request Sent" | "Your Friend" | false>(false);
    useEffect(() => {
        if (authUser) {
            console.log('getting friend status');
            const docRef = db.collection('users').doc(authUser.uid).collection('friends')
                .doc(props.player.uid).get()
                .then((docSnapshot) => {
                    if (docSnapshot.exists) {
                        let data = docSnapshot.data();
                        if (data?.status === 'Pending') {
                            setFriendAdded('Request Sent');
                        }
                        else if (data?.status === 'Friend') {
                            setFriendAdded('Your Friend');
                        }
                    }
                })
        }
    }, [])
    const addFriend = (selfUid: string, friendUid: string) => {
        const addedFriend: addedFriend = {
            uid: friendUid,
            status: 'Pending',
        }
        const docRef = db.collection('users').doc(selfUid).collection('friends')
            .doc(friendUid).set(addedFriend)
            .then(() => {
                console.log(`Friend ${friendUid} was successfully added`);
                setFriendAdded('Request Sent');
            })
            .catch((e) => {
                console.log(e);
                setFriendAdded(false);
            })
    }
    const [alertSnack, setAlertSnack] = useState(false);
    const handleSnackClose = () => {
        setAlertSnack(false);
    }
    const handleSnackOpen = () => {
        console.log("Snack open");
        setAlertSnack(true);
    }
    function Alert(props: AlertProps) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }
    return <>
        <Snackbar open={alertSnack} autoHideDuration={5000} onClose={handleSnackClose}>
            <Alert onClose={handleSnackClose} severity="warning">
                Please sign in to use this feature !
            </Alert>
        </Snackbar>
        <div className={'playerCard card-1'}>
            <div className={'leftInfo'}>
                <div className={'profilePic'}>
                    <img src={player.profilePic} />
                </div>
                <div className={'playerRank'}>
                    <RankImg rankIconName={props.player.rank}/>
                    <div>{player.rank}</div>
                </div>
            </div>
            <div className={'rightInfo'}>
                <div className={'displayName'}>{player.displayName}</div>
                <div className={'filteredInfo'}>
                    <div className={'filterValue'}>
                        <ClockIcon style={{ fill: "#FF5722" }} />
                        <div>{player.hoursPlayed}</div>
                    </div>
                    <div className={'filterValue'}>
                        <ToxicityIcon />
                        <div>65</div>
                    </div>
                    <div className={'filterValue'}>
                        <ToxicityIcon />
                        <div>65</div>
                    </div>
                    {<div className={'filterValue'}>
                        <MicIcon style={{ fill: "#03A9F4" }} />
                        <div>{props.player.mic ? "YES" : "NO"}</div>
                    </div>}
                </div>
                {props.player.roles[0] ? <div className={'playerRoles'}>
                    <div>Role</div>
                    <div>
                        {props.player.roles[0]}
                    </div>
                </div> : ""}
                <div className={'actionBtn'}>
                    {friendAdded ? friendAdded
                        :
                        <Button onClick={() => { authUser ? addFriend(authUser.uid, props.player.uid) : handleSnackOpen() }} variant={'contained'} style={{ color: "#ffffff", background: "rgb(191, 195, 121)" }} color={'secondary'}>Add Friend</Button>
                    }
                </div>
            </div>
        </div>
    </>
}
export default PlayerCard