import { toLower } from 'ionicons/dist/types/components/icon/utils';
import React, { ReactNode } from 'react';

type rankprops = {
    rankIconName : string,
}
const RankImg:React.FC<rankprops> = (props:rankprops) =>{
    // rankIconName has a whitespace but image name is concantenated
    // compensating for this
    const rank = props.rankIconName.split(' ');
    const rankImageName = `${rank[0]}${rank[1]}`;
    console.log("RankImageName ",rankImageName);
    return <>
    <img src={`/images/${rankImageName.toLowerCase()}.png`}/>
    </>
} 
export default RankImg
