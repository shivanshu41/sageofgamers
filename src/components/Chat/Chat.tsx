import React, { useEffect, useContext, useState, useRef } from 'react';
import './Chat.css';
import { ReactComponent as SendIcon } from '../../images/send.svg';
import { AuthContext } from '../../authContext/authContext';
import firebase from '../../firebase';
import { Link } from 'react-router-dom';
import { TargetElement } from '@testing-library/user-event';
import 'animate.css/animate.css';
import { KeyboardInfo } from '@capacitor/core';
const db = firebase.firestore();
interface currentChat {
    to: string,
    from: string,
    toName?: string,
    fromName?: string,
    messages?: message[]
}
type message = {
    timestamp: firebase.firestore.Timestamp
    message: string,
    nature: 'recepient' | 'sender',
}
const Chat = () => {
    const chatInputRef = useRef<HTMLInputElement>(null);
    const { authUser } = useContext(AuthContext);
    const [friends, setFriends] = useState<any[]>([]);
    const [preRender, setPreRender] = useState<boolean>(true);
    const [currChat, setCurrChat] = useState<currentChat | null>(null);
    useEffect(() => {
        if (!authUser) return;
        const docRef = db.collection('users').doc(authUser.uid).collection('friends')
            .where('status', '==', 'Friend')
            .get()
            .then((colSnapshot) => {
                if (!colSnapshot.empty) {
                    let docs = colSnapshot.docs;
                    docs.forEach((doc, index) => {
                        let data = doc.data();
                        console.log(data);
                        db.collection('users').doc(data.uid).get()
                            .then((docSnap) => {
                                if (docSnap.exists) {
                                    setFriends((prevState) => {
                                        return prevState?.concat(docSnap.data());
                                    })
                                    setPreRender(false);
                                }
                            })
                            .catch((e) => {
                                console.log(e);
                                setFriends([]);
                                setPreRender(false);
                            })
                    })
                } else {
                    setPreRender(false);
                }
            })
            .catch((e) => {
                console.log(e);
                setFriends([]);
                setPreRender(false);
            })
    }, [authUser]);
    const handleChat = (Event: React.SyntheticEvent) => {
        let friendUid = Event.currentTarget.attributes['data-uid' as any].value;
        let friendName = Event.currentTarget.attributes['data-name' as any].value;
        // it is advised to detach the previous snapshot listener
        setCurrChat({
            to: friendUid,
            from: authUser.uid,
            toName: friendName,
        })
        attachChatListener(friendUid);

    }
    const attachChatListener = (friendUid: string) => {
        db.collection('users').doc(authUser.uid).collection('friends').doc(friendUid)
            .onSnapshot((doc) => {
                if (doc.exists) {
                    console.log('doc is ', doc.data());
                    setCurrChat((prevState: any) => {
                        return { ...prevState, messages: doc.data()?.chatMessages }
                    })
                    let element = document.getElementById('tobottom');
                    element?.scrollIntoView();
                }
            })
    }
    const handleFriends = friends.map((value, index) => {
        return <div key={`friend_${index}`} data-name={value.displayName} data-uid={value.uid} onClick={(e) => handleChat(e)} className={'friend'}>
            <div className={'friendPic'}>
                <img src={value.avatar} />
            </div>
            <div className={'friendName'}>{value.displayName}</div>
            <div className={'newMessage'}>65</div>
        </div>
    })
    const handleChatInput = () => {

        let msg = chatInputRef.current!.value;
        let senderChatMessage: message = {
            nature: "sender",
            timestamp: firebase.firestore.Timestamp.now(),
            message: msg
        }
        let recepientChatMessage: message = {
            nature: "recepient",
            timestamp: firebase.firestore.Timestamp.now(),
            message: msg
        }
        if (currChat && msg !=="") {
            // adding the chat message to self collection
            db.collection('users').doc(authUser.uid).collection('friends').doc(currChat.to)
                .update({
                    chatMessages: firebase.firestore.FieldValue.arrayUnion(senderChatMessage)
                })
                .then(() => {
                    let element = document.getElementById('tobottom');
                    element?.scrollIntoView();
                })
            // adding the chat message to recepient collection
            db.collection('users').doc(currChat.to).collection('friends').doc(authUser.uid)
                .update({
                    chatMessages: firebase.firestore.FieldValue.arrayUnion(recepientChatMessage)
                })
            // Clearing input value
            chatInputRef.current!.value = "";
        }
    }
    const handleEnterPress = (event: any) => {
        let code = event.keyCode || event.which;
        let name = event.key;
        if (code === 13) {
            //13 is the enter keycode
            handleChatInput();
        }
    }
    const isReadyToRender = () => {
        if (preRender) {
            return <div className="spinner" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%,-50%)" }} />
        } else {
            return <div className={'friendsChatContainer'}>
                <div className={'card-2'} style={{ display: 'flex', height: '100%', background: "#fcfcfc", borderRadius: '5px' }}>
                    <div className={'friendsList'}>
                        {friends.length === 0 ?
                            <div className='friendFallback'>
                                <div className={'friendFallbackHead'}>Oops</div>
                                <div className={'friendFallbackSubHead'}>It looks like you don't have friends</div>
                                <div className={'friendFallbackSubHead'}>Find some friends <Link to={'/players'}>here</Link> and come back again 😁</div>
                            </div> : handleFriends}
                    </div>
                    <div className={'chatBoxContainer'}>
                        <div className={'chatTitle'}>
                            {currChat?.toName ? currChat?.toName : 'Chat'}
                        </div>
                        <div className={'chatBox'}>
                            <div className={'messages'}>
                                {currChat?.messages?.map((value, index) => {
                                    let date = new Date;
                                    return <div key={`msg_${index}`} className={value.nature === 'recepient' ? 'animate_animated animate_bounceIn  recipientMessage' : 'animate_animated animate_bounceIn  senderMessage'}>
                                        <div className={`msg ${value.nature === 'recepient' ? 'msgRec' : 'msgSend'}`}>
                                            {value.message}
                                        </div>
                                        <div className={'date'}>
                                            {value.timestamp.toDate().toDateString()}
                                        </div>
                                    </div>
                                })}
                                <div id={'tobottom'}></div>
                            </div>
                            <div className={'messageBox'}>
                                <div className={'messageInput'}>
                                    <input onKeyPress={(e) => handleEnterPress(e)} disabled={currChat ? false : true} ref={chatInputRef} type={'text'} placeholder={'enter your message'} />
                                </div>
                                <div className={'sendButton'}>
                                    <SendIcon onClick={handleChatInput} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        }
    }
    return <>
        <div className={'chatContainer'}>
            {isReadyToRender()}
        </div>
    </>
}

export default Chat;