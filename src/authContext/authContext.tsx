import React, { useState ,useMemo,useEffect,useLayoutEffect} from 'react';
import url from 'url';
import {useLocation} from 'react-router-dom';
import firebase from '../firebase';
const db = firebase.firestore();
const AuthContext = React.createContext<any | null>('defaultUser');

const AuthProvider:React.FC =(props)=>{
    let location = window.location.href;
    const [authUser,setAuthUser] = useState<null | any>(null);
    useLayoutEffect(()=>{
        firebase.auth().onAuthStateChanged((user)=>{
            if(user){
                console.log("fire Auth user present",user);
                setAuthUser(user);
            }else{
                console.log("fire Auth user not present");
                setAuthUser(null);
            }
        })
        const qs = url.parse(location,true);
        console.log('token',qs.query.token);
        if(qs.query.token){
            firebase.auth().signInWithCustomToken(qs.query.token.toString())
            .then(res=>{
                console.log(res);
            })
            .catch(e=>{
                console.log(e);
            })
        }
    },[authUser])
    return(
        <AuthContext.Provider value={{authUser}}>
            {props.children}
        </AuthContext.Provider>
    )
}  
export default AuthProvider;
export {AuthContext};
